import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';

import 'mobx/movies_controller.dart';
import 'router/app_router.gr.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();

  // GetIt singleton for MoviesController
  GetIt.I.registerSingleton<MoviesController>(MoviesController());

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  MyApp({Key? key}) : super(key: key);

  // AutoRoute
  final _appRouter = AppRouter();

  @override
  Widget build(BuildContext context) {
    return MaterialApp.router(
      debugShowCheckedModeBanner: false,
      routerDelegate: _appRouter.delegate(),
      routeInformationParser: _appRouter.defaultRouteParser(),
    );
  }
}
