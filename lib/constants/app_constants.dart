class AppConstants {
  static const String dbName = 'movies_collection';
  static const int dbVersion = 1;
  static const String tableMovies = 'movies';
  static const String columnId = '_id';
  static const String columnTitle = 'title';
  static const String columnDirector = 'director';
  static const String columnSummary = 'summary';
  static const String columnGenre = 'genre';

  static const List<String> genres = [
    'Drama',
    'Action',
    'Animation',
    'Sci-Fi',
    'Horror'
  ];
}
