import 'package:auto_route/annotations.dart';

import '../screens/home.dart';
import '../screens/movie_details.dart';

@MaterialAutoRouter(
  replaceInRouteName: 'Page,Route',
  routes: <AutoRoute>[
    AutoRoute(page: Home, initial: true),
    AutoRoute(page: MovieDetails),
  ],
)
// extend the generated private router
class $AppRouter {}
