// **************************************************************************
// AutoRouteGenerator
// **************************************************************************

// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouteGenerator
// **************************************************************************
//
// ignore_for_file: type=lint

import 'package:auto_route/auto_route.dart' as _i3;
import 'package:flutter/material.dart' as _i4;

import '../mobx/movies_controller.dart' as _i6;
import '../models/movie.dart' as _i5;
import '../screens/home.dart' as _i1;
import '../screens/movie_details.dart' as _i2;

class AppRouter extends _i3.RootStackRouter {
  AppRouter([_i4.GlobalKey<_i4.NavigatorState>? navigatorKey])
      : super(navigatorKey);

  @override
  final Map<String, _i3.PageFactory> pagesMap = {
    Home.name: (routeData) {
      return _i3.MaterialPageX<dynamic>(
          routeData: routeData, child: const _i1.Home());
    },
    MovieDetails.name: (routeData) {
      final args = routeData.argsAs<MovieDetailsArgs>(
          orElse: () => const MovieDetailsArgs());
      return _i3.MaterialPageX<dynamic>(
          routeData: routeData,
          child: _i2.MovieDetails(
              key: args.key,
              showDeleteButton: args.showDeleteButton,
              title: args.title,
              movie: args.movie,
              controller: args.controller));
    }
  };

  @override
  List<_i3.RouteConfig> get routes => [
        _i3.RouteConfig(Home.name, path: '/'),
        _i3.RouteConfig(MovieDetails.name, path: '/movie-details')
      ];
}

/// generated route for
/// [_i1.Home]
class Home extends _i3.PageRouteInfo<void> {
  const Home() : super(Home.name, path: '/');

  static const String name = 'Home';
}

/// generated route for
/// [_i2.MovieDetails]
class MovieDetails extends _i3.PageRouteInfo<MovieDetailsArgs> {
  MovieDetails(
      {_i4.Key? key,
      bool showDeleteButton = false,
      String title = 'Movie Details',
      _i5.Movie? movie,
      _i6.MoviesController? controller})
      : super(MovieDetails.name,
            path: '/movie-details',
            args: MovieDetailsArgs(
                key: key,
                showDeleteButton: showDeleteButton,
                title: title,
                movie: movie,
                controller: controller));

  static const String name = 'MovieDetails';
}

class MovieDetailsArgs {
  const MovieDetailsArgs(
      {this.key,
      this.showDeleteButton = false,
      this.title = 'Movie Details',
      this.movie,
      this.controller});

  final _i4.Key? key;

  final bool showDeleteButton;

  final String title;

  final _i5.Movie? movie;

  final _i6.MoviesController? controller;

  @override
  String toString() {
    return 'MovieDetailsArgs{key: $key, showDeleteButton: $showDeleteButton, title: $title, movie: $movie, controller: $controller}';
  }
}
