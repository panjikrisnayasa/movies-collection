import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';

import '../constants/app_constants.dart';
import '../database/database_helper.dart';
import '../mobx/movies_controller.dart';
import '../models/movie.dart';
import '../theme.dart';

class MovieDetails extends StatefulWidget {
  const MovieDetails({
    Key? key,
    this.showDeleteButton = false,
    this.title = 'Movie Details',
    this.movie,
    this.controller,
  }) : super(key: key);

  final bool showDeleteButton;
  final String title;
  final Movie? movie;
  final MoviesController? controller;

  @override
  State<MovieDetails> createState() => _MovieDetailsState();
}

class _MovieDetailsState extends State<MovieDetails> {
  final _formKey = GlobalKey<FormState>();
  final _dbHelper = DatabaseHelper.instance;
  final _moviesController = GetIt.I<MoviesController>();

  final _title = TextEditingController();
  final _director = TextEditingController();
  final _summary = TextEditingController();

  String? _selectedGenre;
  bool _showSelectGenreError = false;

  @override
  void initState() {
    super.initState();

    // instantiate movie details form with the movie data
    if (widget.movie != null) {
      _title.text = widget.movie!.title;
      _director.text = widget.movie!.director;
      _summary.text = widget.movie!.summary;
      _selectedGenre = widget.movie!.genre;
    }
  }

  _onSelectGenre(String genre) {
    setState(() {
      _selectedGenre = genre;
    });
  }

  _onSave() async {
    setState(() {
      _showSelectGenreError = _selectedGenre == null;
    });

    if (_formKey.currentState!.validate()) {
      // validation for genre
      if (_selectedGenre != null) {
        var movie = Movie(
          id: widget.movie?.id,
          title: _title.text,
          director: _director.text,
          summary: _summary.text,
          genre: _selectedGenre!,
        );

        if (widget.movie == null) {
          // insert new movie
          await _dbHelper.insert(movie).then((value) async {
            if (value != null) {
              ScaffoldMessenger.of(context).showSnackBar(
                  const SnackBar(content: Text('Movie added successfully')));
              await _moviesController.refreshMovies();
              context.router.pop();
            }
          });
        } else {
          // update movie
          await _dbHelper.update(movie).then((value) async {
            if (value != null) {
              await _moviesController.refreshMovies();
              ScaffoldMessenger.of(context).showSnackBar(
                  const SnackBar(content: Text('Movie updated successfully')));
            }
          });
        }
      }
    }
  }

  _onDelete() async {
    await _dbHelper.delete(widget.movie!.id!).then((value) async {
      if (value != null) {
        ScaffoldMessenger.of(context).showSnackBar(
            const SnackBar(content: Text('Movie deleted successfully')));
        await _moviesController.refreshMovies();
        context.router.popUntilRoot();
      }
    });
  }

  Future<void> _showDeleteConfirmation() async {
    return showDialog<void>(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: const Text('Delete Movie'),
          content: const Text('Are you sure want to delete this movie?'),
          actions: <Widget>[
            TextButton(
              child: const Text('Yes'),
              onPressed: _onDelete,
            ),
            TextButton(
              child: const Text('No', style: TextStyle(color: Colors.grey)),
              onPressed: () {
                context.router.pop();
              },
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
        actions: [
          if (widget.showDeleteButton)
            IconButton(
              icon: const Icon(Icons.delete),
              onPressed: () => _showDeleteConfirmation(),
            ),
          IconButton(
            icon: const Icon(Icons.save),
            onPressed: () => _onSave(),
          ),
        ],
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(24),
            child: Form(
              key: _formKey,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  // title text field
                  TextFormField(
                    autofocus: true,
                    decoration: const InputDecoration(
                      labelText: 'Title',
                    ),
                    controller: _title,
                    textCapitalization: TextCapitalization.words,
                    keyboardType: TextInputType.text,
                    validator: (value) {
                      if (value!.isEmpty) {
                        return 'This field is required';
                      }
                      return null;
                    },
                  ),
                  const SizedBox(
                    height: 8,
                  ),

                  // director text field
                  TextFormField(
                    decoration: const InputDecoration(
                      labelText: 'Director',
                    ),
                    controller: _director,
                    textCapitalization: TextCapitalization.words,
                    keyboardType: TextInputType.text,
                    validator: (value) {
                      if (value!.isEmpty) {
                        return 'This field is required';
                      }
                      return null;
                    },
                  ),
                  const SizedBox(
                    height: 8,
                  ),

                  // summary text field
                  TextFormField(
                    decoration: const InputDecoration(
                      labelText: 'Summary',
                    ),
                    controller: _summary,
                    textCapitalization: TextCapitalization.sentences,
                    keyboardType: TextInputType.text,
                    maxLength: 100,
                    maxLines: 3,
                    validator: (value) {
                      if (value!.isEmpty) {
                        return 'This field is required';
                      }
                      return null;
                    },
                  ),
                  const SizedBox(
                    height: 32,
                  ),

                  // select genre field
                  Wrap(
                    spacing: 16,
                    children: List<Widget>.generate(
                      AppConstants.genres.length,
                      (int index) {
                        return ChoiceChip(
                          padding: const EdgeInsets.symmetric(horizontal: 8),
                          selectedColor: Colors.blue[200],
                          label: Text(AppConstants.genres[index]),
                          onSelected: (_) =>
                              _onSelectGenre(AppConstants.genres[index]),
                          selected: _selectedGenre != null &&
                              _selectedGenre == AppConstants.genres[index],
                        );
                      },
                    ).toList(),
                  ),

                  // select genre error
                  if (_showSelectGenreError)
                    Text(
                      'Please select a genre',
                      style: textError,
                    ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
