import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:get_it/get_it.dart';

import '../mobx/movies_controller.dart';
import '../router/app_router.gr.dart';
import '../widgets/movie_card.dart';

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  final _search = TextEditingController();
  final _moviesController = GetIt.I<MoviesController>();

  @override
  void initState() {
    super.initState();

    _refreshMovies();

    _search.addListener(() async {
      await _moviesController.filterMovies(_search.text);
    });
  }

  _refreshMovies() async {
    await _moviesController.refreshMovies();
  }

  @override
  Widget build(BuildContext context) {
    final router = AutoRouter.of(context);

    return Scaffold(
      appBar: AppBar(
        title: const Text('Movies Collection'),
      ),
      body: SafeArea(
        child: Center(
          child: Padding(
            padding: const EdgeInsets.all(16),
            child: Column(
              children: [
                TextFormField(
                  decoration: const InputDecoration(
                    hintText: 'Search by title...',
                  ),
                  controller: _search,
                ),
                const SizedBox(
                  height: 32,
                ),
                Observer(
                  builder: (context) {
                    if (_moviesController.movies.isNotEmpty) {
                      return Expanded(
                        child: ListView.builder(
                          itemBuilder: (context, count) {
                            return MovieCard(
                              movie: _moviesController.movies[count],
                              onTap: () {
                                router.push(MovieDetails(
                                  showDeleteButton: true,
                                  movie: _moviesController.movies[count],
                                ));
                              },
                            );
                          },
                          itemCount: _moviesController.movies.length,
                        ),
                      );
                    } else {
                      return const Padding(
                        padding: EdgeInsets.only(top: 32),
                        child: Text('No movies found'),
                      );
                    }
                  },
                )
              ],
            ),
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          router.push(MovieDetails(title: 'Add Movie'));
        },
        tooltip: 'Add movie',
        child: const Icon(Icons.add),
      ),
    );
  }
}
