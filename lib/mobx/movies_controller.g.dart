// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'movies_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$MoviesController on MoviesControllerStore, Store {
  late final _$moviesAtom =
      Atom(name: 'MoviesControllerStore.movies', context: context);

  @override
  List<Movie> get movies {
    _$moviesAtom.reportRead();
    return super.movies;
  }

  @override
  set movies(List<Movie> value) {
    _$moviesAtom.reportWrite(value, super.movies, () {
      super.movies = value;
    });
  }

  late final _$refreshMoviesAsyncAction =
      AsyncAction('MoviesControllerStore.refreshMovies', context: context);

  @override
  Future<void> refreshMovies() {
    return _$refreshMoviesAsyncAction.run(() => super.refreshMovies());
  }

  late final _$filterMoviesAsyncAction =
      AsyncAction('MoviesControllerStore.filterMovies', context: context);

  @override
  Future<void> filterMovies(String input) {
    return _$filterMoviesAsyncAction.run(() => super.filterMovies(input));
  }

  @override
  String toString() {
    return '''
movies: ${movies}
    ''';
  }
}
