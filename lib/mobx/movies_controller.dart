import 'package:mobx/mobx.dart';

import '../database/database_helper.dart';
import '../models/movie.dart';

part 'movies_controller.g.dart';

class MoviesController = MoviesControllerStore with _$MoviesController;

abstract class MoviesControllerStore with Store {
  final _dbHelper = DatabaseHelper.instance;

  @observable
  List<Movie> movies = [];

  @action
  Future<void> refreshMovies() async {
    await _dbHelper.getMovies().then((value) {
      movies = value;
    });
  }

  @action
  Future<void> filterMovies(String input) async {
    await _dbHelper.filter(input).then((value) {
      movies = value;
    });
  }
}
