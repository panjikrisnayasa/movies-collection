import 'package:flutter/material.dart';

TextStyle textError = const TextStyle(
  color: Colors.red,
  fontSize: 12,
);
