import 'dart:io';

import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';

import '../constants/app_constants.dart';
import '../models/movie.dart';

class DatabaseHelper {
  DatabaseHelper._privateConstructor();
  static final DatabaseHelper instance = DatabaseHelper._privateConstructor();

  static Database? _database;
  Future<Database?> get database async {
    _database ??= await _initDatabase();
    return _database;
  }

  _initDatabase() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, AppConstants.dbName);
    return await openDatabase(path,
        version: AppConstants.dbVersion, onCreate: _onCreate);
  }

  Future _onCreate(Database db, int version) async {
    await db.execute('''
      CREATE TABLE ${AppConstants.tableMovies} (
        ${AppConstants.columnId} INTEGER PRIMARY KEY AUTOINCREMENT,
        ${AppConstants.columnTitle} TEXT,
        ${AppConstants.columnDirector} TEXT,
        ${AppConstants.columnSummary} VARCHAR(100),
        ${AppConstants.columnGenre} TEXT)
      ''');
  }

  Future<List<Movie>> getMovies() async {
    var db = await instance.database;
    var movies = await db?.query(AppConstants.tableMovies,
        orderBy: AppConstants.columnId);
    List<Movie> movieList = movies != null && movies.isNotEmpty
        ? movies.map((movie) => Movie.fromMap(movie)).toList()
        : [];
    return movieList;
  }

  Future<int?> insert(Movie movie) async {
    var db = await instance.database;
    return await db?.insert(AppConstants.tableMovies, movie.toMap());
  }

  Future<int?> delete(int id) async {
    var db = await instance.database;
    return await db?.delete(AppConstants.tableMovies,
        where: '${AppConstants.columnId} = ?', whereArgs: [id]);
  }

  Future<int?> update(Movie movie) async {
    var db = await instance.database;
    return await db?.update(AppConstants.tableMovies, movie.toMap(),
        where: '${AppConstants.columnId} = ?', whereArgs: [movie.id]);
  }

  Future<List<Movie>> filter(String input) async {
    var db = await instance.database;
    var movies = await db?.query(AppConstants.tableMovies,
        where: '${AppConstants.columnTitle} LIKE ?', whereArgs: ['%$input%']);
    List<Movie> movieList = movies != null && movies.isNotEmpty
        ? movies.map((movie) => Movie.fromMap(movie)).toList()
        : [];
    return movieList;
  }
}
