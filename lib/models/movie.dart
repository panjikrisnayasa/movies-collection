import 'dart:convert';

class Movie {
  int? id;
  String title;
  String director;
  String summary;
  String genre;

  Movie({
    this.id,
    required this.title,
    required this.director,
    required this.summary,
    required this.genre,
  });

  Movie copyWith({
    int? id,
    String? title,
    String? director,
    String? summary,
    String? genre,
  }) {
    return Movie(
      id: id ?? this.id,
      title: title ?? this.title,
      director: director ?? this.director,
      summary: summary ?? this.summary,
      genre: genre ?? this.genre,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      '_id': id,
      'title': title,
      'director': director,
      'summary': summary,
      'genre': genre,
    };
  }

  factory Movie.fromMap(Map<String, dynamic> map) {
    return Movie(
      id: map['_id']?.toInt() ?? 0,
      title: map['title'] ?? '',
      director: map['director'] ?? '',
      summary: map['summary'] ?? '',
      genre: map['genre'] ?? '',
    );
  }

  String toJson() => json.encode(toMap());

  factory Movie.fromJson(String source) => Movie.fromMap(json.decode(source));

  @override
  String toString() {
    return 'Movie(id: $id, title: $title, director: $director, summary: $summary, genre: $genre)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is Movie &&
        other.id == id &&
        other.title == title &&
        other.director == director &&
        other.summary == summary &&
        other.genre == genre;
  }

  @override
  int get hashCode {
    return id.hashCode ^
        title.hashCode ^
        director.hashCode ^
        summary.hashCode ^
        genre.hashCode;
  }
}
