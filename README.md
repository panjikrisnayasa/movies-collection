# Movies Collection

A simple CRUD app for saving a list of movie collection.

This project uses SQFLite for the database,
and some other libraries used:
- AutoRoute
- MobX
- GetIt

## App Demo

https://drive.google.com/file/d/1xrakQTnuoZC6gt0ex9jE81MI74AtdEMF/view?usp=sharing

## Getting Started

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.
